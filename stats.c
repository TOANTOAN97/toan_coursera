/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file <Add File Name> 
 * @brief <Add Brief Description Here >
 *
 * <Add Extended Description Here>
 *
 * @author <Add FirsName LastName>
 * @date <Add date >
 *
 */



#include <stdio.h>
#include <string.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */
  /* Statistics and Printing Functions Go Here */
	print_array(test, SIZE);
	print_statistics(test);

}


/* Add other Implementation File Code Here */
void print_array(unsigned char *parr, int size){
	
	for(int i = 0; i < size; i++){
		printf("%d \n", *(parr + i));
	}
}

unsigned char find_maximum(unsigned char *parr, int size){
	unsigned char max = 0;
	for(int i = 0; i < size; i++){
		if(max >= *(parr + i));
		else max = *(parr + i);
	}
	return max;
}
unsigned char find_minimum(unsigned char *parr, int size){
	unsigned char min = 255;
	for(int i = 0; i < size; i++){
		if(min < *(parr + i));
		else min = *(parr + i);	
	}
	return min;
}
float find_median(unsigned char *parr, int size){
	float median;
	if(size % 2 ==0){
			median = (*(parr + size/2 -1) + *(parr + size/2))/2.0;
	}else median = *(parr + size/2);
	return median;
}
float find_mean(unsigned char *parr, int size){
	float mean = 0;
	int S = 0;
	for(int i = 0; i < size; i++){
		S += *(parr + i);
	}
	mean = (float)S/size;
	return mean;
}
void sort_array(unsigned char *parr, int size){
	unsigned char temp;
	int i, j;
	for(i = 0; i < size - 1; i++){
		for(j = i + 1; j < size; j++){
			if(*(parr + i) >= *(parr + j));
			else{
				temp = *(parr + i);
				*(parr + i) = *(parr + j);
				*(parr + j) = temp;
			}
		}	
	}
	print_array(parr, 40);
}
void print_statistics(unsigned char *parr){
	printf("%f \n", find_median(parr, SIZE));
	printf("%f \n", find_mean(parr, SIZE));
	printf("%d \n", find_minimum(parr, SIZE));
	printf("%d \n", find_maximum(parr, SIZE));
}
